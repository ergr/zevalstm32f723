# Evaluate STM32F732 Discovery Board

Used resources to follow:

## Board resources

[ST-Product Page 32F723EDISCOVERY](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-discovery-kits/32f723ediscovery.html)

[User Manual](https://www.st.com/resource/en/user_manual/dm00342318-discovery-kit-with-stm32f723ie-mcu-stmicroelectronics.pdf)
[Getting started](https://www.st.com/resource/en/user_manual/dm00285842-getting-started-with-stm32-mcu-discovery-kits-software-development-tools-stmicroelectronics.pdf)

[ST Link V2 firmware update](https://my.st.com/content/my_st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stsw-link007.html#get-software)
Check the readme.txt

    sudo apt-get install libusb-1.0
    java -jar STLinkUpgrade.jar
    
media/STLinkUpgrade_STM32F723Discovery_01_StLink_BeforeUpdate.png

media/STLinkUpgrade_STM32F723Discovery_02_StLink_AfterUpdate.png

## Build tool-chain - The Sven Way 

2. IDE
2.1. openstm32.org
Die IDE basiert auf eclipe.
Gibt's für Win, Mac und Linux
Für den Download muss man sich registrieren.
Die Installation kann auf unterschiedlichen Wegen erreicht werden.
Ich habe mich für die Komplettinstallation entschieden.
Der Download des Installers gibt's hier:
https://www.openstm32.org/Downloading%2Bthe%2BSystem%2BWorkbench%2Bfor%2BSTM32%2Binstaller

Speziell für Linux ist das hier:
http://www.ac6-tools.com/downloads/SW4STM32/install_sw4stm32_linux_64bits-v2.9.run

3. STM32 Software und Code Examples
3.1. STM32CubeF7 Software Sammlung
ST hat die ganze Software (HAL ..) plus Beispiele und was sonst noch gibt zusammen gepackt in dem Paket "STM32CubeF7"
Siehe hier
https://www.st.com/content/st_com/en/products/embedded-software/mcu-mpu-embedded-software/stm32-embedded-software/stm32cube-mcu-mpu-packages/stm32cubef7.html

Der Download ist etwas tricky.
Bei mir hat Button "Get software" nicht funktioniert.
Aber in der gleichen Zeile ganz vorne ist ein kleines "+"
Wenn man da drauf klickt findet man einen Link zu Github.
Siehe hier:
https://github.com/STMicroelectronics/STM32CubeF7

Hier kann das STM32CubeF7 herunter geladen werden.
Allerdings hat bei mir der Download der ZIP-Datei auch nicht geklappt und ist immer mit Fehler abgebrochen.

Richtig gut funktioniert hat allerdings ein Clone über GIT mit
"git clone https://github.com/STMicroelectronics/STM32CubeF7.git"
Ist allerdings sehr langsam.

3.2. STM32CubeF7 Example "DAC_SignalsGeneration"
3.2.1. Projekt importieren und kompilieren
Das Beispiel kann so wie es ist direkt in die eclipse Oberfläche importiert werden.
Siehe dazu das oben aufgeführte Dokument "Getting Started"
https://www.st.com/resource/en/user_manual/dm00285842-getting-started-with-stm32-mcu-discovery-kits-software-development-tools-stmicroelectronics.pdf
Seite 17 (Kapitel 3.4)
Das Projekt sollte jetzt ohne Fehler und Warnings kompilieren.

3.2.2. Upload und Debugging
Nach dem Bauen kann das Binary des Beispiel-Projektes auf das Disco-Board geladen werden.
Dazu F11 drücken oder aus dem Menü "Run/Debug" auswählen
Jetzt fragt die IDE welche Debug Konfiguration geladen werden soll.
Bei mir hat es funktioniert mit der Konfig "Ac6 STM32 Config"
Allerdings kam erst ein Gemecker weil irgend etwas nicht passt.
Beim Lesen der Fehlermeldung wurde klar, das die "libncurses5" für 32-Bit fehlt.
Also nachinstalliert mit "sudo apt-get install libncurses5:i386"
Damit läuft es dann bei mir.
Das neu hoch geladene Projekt bleibt auch erhalten, wenn die Stromzufuhr am Board unterbrochen wurde.
Es wird nach Strom anlegen das zuletzt hochgeladene Projekt gebootet.
D.h. die IDE spielt das Programm scheinbar in den Flash Mem.



## Build tool-chain - gnu-mcu-eclipse

https://gnu-mcu-eclipse.github.io/


## Build tool-chain - CubeMX + CodeBlocks

[instructables curcuits - Build a Program for STM32 MCU Under Linux](https://www.instructables.com/id/Build-a-Program-for-STM32-MCU-Under-Linux/)

[STM32Cube Ecosystem](https://www.st.com/content/st_com/en/stm32cube-ecosystem.html)

[STM32Cube MCU Package for STM32F7 series ](https://www.st.com/en/embedded-software/stm32cubef7.html#get-software)


here are the Ubuntu installation items related to the "inscturctables curcuits" page.

    sudo apt-get install gcc-arm-none-eabi binutils-arm-none-eabi libnewlib-arm-none-eabi
    
    sudo apt install git
    
    sudo apt-get install codeblocks
    
    sudo apt install stlink-tools stlink-gui
    
    git clone https://github.com/duro80/Makefile4CubeMX.git

    sudo ln -s "$PWD"/CubeMX2Makefile.py /usr/bin/CubeMX2Makefile.py
    

## Build tool-chain - Atolic Studio

[Programming STM32 on Linux](https://medium.com/@olayiwolaayinde/programming-stm32-on-linux-d6a6ee7a8d8d)     
    
